FROM ruby:2.6-alpine

ENV WD /home/daemon

RUN apk --update add --virtual build_deps \
    build-base ruby-dev libc-dev linux-headers \
    openssl-dev postgresql-dev libxml2-dev libxslt-dev mariadb-dev \
    && mkdir -p $WD

WORKDIR $WD

COPY Gemfile Gemfile.lock ./

RUN time bundler install

COPY . .

RUN apk del build_deps && chown daemon:daemon -R . && ./scripts/ci_generate_config.sh && cat localconf.yml

USER daemon

CMD [ "echo", "hi", "&&", "printenv" ]
