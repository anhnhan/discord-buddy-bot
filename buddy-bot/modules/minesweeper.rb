
module BuddyBot::Modules::Minesweeper
  @@default_width = 7
  @@default_height = 7
  @@default_bombs = 6

  @@number_emojis = [ ":zero:", ":one:", ":two:", ":three:", ":four:", ":five:", ":six:", ":seven:", ":eight:", ":nine:", ":keycap_ten:" ]

  @@bomb_emotes = [
    [
      560768460297207808, # :sowonheart:
      614621503584993281, # :sowonsneeze:
    ],
    [
      443338845874814977, # :ayerinpeek:
      351908025289801739, # :yerinheart:
      471761896115011585, # :yerinzzz:
    ],
    [
      434376562142478367, # :eunhashock:
      430174078381326336, # :eunhacool:
      428641542459097089, # :eunhasip:
      678811666506579979, # :eunhagang:
    ],
    [
      424034445780320256, # :yujuomo:
      285243398422659073, # :yujuthumbsup:
    ],
    [
      480162492694396928, # :sinbtongue:
      506095800447795220, # :sinbhi:
    ],
    [
      398700581377671168, # :umjinonono:
      426438650914668545, # :aumjisip:
    ],
  ]

  @@dud_marker = ":white_large_square:"

  def self.generate_playboard(w = @@default_width, h = @@default_height, amount = @@default_bombs)
    board = Array.new(h) { Array.new(w, @@dud_marker) }

    # insert bombs
    self.add_bombs(board, amount)

    # insert bomb hints
    self.add_bomb_hints(board)

    return board
  end

  def self.add_bombs(board, amount)
    inserted_bombs = 0
    loop do
      bomb_x = rand(board.length)
      bomb_y = rand(board[0].length)

      next if board[bomb_x][bomb_y] != @@dud_marker

      bomb_emote_index = inserted_bombs % @@bomb_emotes.length
      bomb_emote = BuddyBot.emoji(@@bomb_emotes[bomb_emote_index].sample)
      board[bomb_x][bomb_y] = "#{bomb_emote}"

      inserted_bombs += 1
      break if inserted_bombs == amount
    end
  end

  def self.add_bomb_hints(board)
    x = 0
    loop do
      y = -1
      loop do
        y += 1
        break if y == board[0].length
        next if board[x][y] != @@dud_marker

        count = 0

        # row above
        count += self.check_bomb(x - 1, y - 1, board, dud_marker = @@dud_marker)
        count += self.check_bomb(x - 1, y, board, dud_marker = @@dud_marker)
        count += self.check_bomb(x - 1, y + 1, board, dud_marker = @@dud_marker)

        # same row
        count += self.check_bomb(x, y - 1, board, dud_marker = @@dud_marker)
        count += self.check_bomb(x, y + 1, board, dud_marker = @@dud_marker)

        # row below
        count += self.check_bomb(x + 1, y - 1, board, dud_marker = @@dud_marker)
        count += self.check_bomb(x + 1, y, board, dud_marker = @@dud_marker)
        count += self.check_bomb(x + 1, y + 1, board, dud_marker = @@dud_marker)

        next if count == 0
        board[x][y] = @@number_emojis[count]
      end
      x += 1
      break if x == board.length
    end
  end

  def self.check_bomb(x, y, board, dud_marker = @@dud_marker)
    if x < 0 || y < 0
      return 0
    end

    if x >= board.length || y >= board[0].length
      return 0
    end

    return board[x][y][0] == "<" ? 1 : 0
  end

  def self.board_to_string(board)
    result = ""

    board.each do |row|
      row.each do |cell|
        result << "||#{cell}||"
      end
      result << "\n"
    end

    return result
  end
end
